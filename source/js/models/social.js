var Social = Backbone.Model.extend({
    defaults: {
        auth_url: "",
        code: "",
        color: "",
        icon: "",
        is_attached: false,
        redirect_url_code_key: "",
        title: "",
        updated_at: false,
        url: ""
    }
});

var Socials = Backbone.Collection.extend({
    model: Social,

    initialize: function() {
    },

    fetch: function(callback) {
        callback = callback || function() {};
        var self = this;
        socrate.listUserAuthTypes(user.get('auth_token'), true, function(data, success, errors) {
            if (success) {
                var shortCodes = {
                    'foursquare' : 'fs',
                    'vk' : 'vk',
                    'twitter' : 'tw',
                    'facebook' : 'fb'
                };

                data = data.map(function(dataItem) {
                    dataItem.short_code = shortCodes[dataItem.code];
                    return dataItem;
                });
                self.reset(data);
                callback();
            } else {
                handleSocketErrors(errors);
            }
        });
    }
});