var Metric = Backbone.Model.extend({
    defaults: {
        code: '',
        title: '',
        icon: '',
        place: 0,
        level: 1,
        scores: 0,
        next_level_scores: 1
    }
});

var Metrics = Backbone.Collection.extend({
    model: Metric
});