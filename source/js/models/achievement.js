var Achievement = Backbone.Model.extend({
    defaults: {
        code: '',
        title: '',
        icon: '',
        place: 0,
        level: 1,
        scores: 0,
        next_level_scores: 0
    }
});

var Achievements = Backbone.Collection.extend({
    model: Achievement
});