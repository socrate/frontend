var TotalMetric = Backbone.Model.extend({
    defaults: {
        place: 0,
        level: 1,
        scores: 0,
        next_level_scores: 0
    }
});