var Profile = Backbone.Model.extend({
    defaults: {
        id: 0,
        name: '',
        last_name: '',
        avatar: '',
        total_metric: null,
        metrics: new Metrics()
    },

    initialize: function() {
        _.bindAll(this, 'fetch');
        this.set({
            total_metric: new TotalMetric(),
        }, {
            silent: true
        });
    },

    fetch: function(callback) {
        callback = callback || function() {};

        var auth_token = user.get('auth_token');
        var user_id = user.get('user_id');
        var user_login = user.get('user_login');

        var self = this;
        socrate.getUserProfile(auth_token, user_id, user_login, function(data, success, errors) {
            if (success) {
                self.set(_(data).omit('metrics', 'total_metric'));
                self.get('total_metric').set(data.total_metric);
                self.get('metrics').reset(data.metrics);
                callback();
            } else {
                handleSocketErrors(errors);
            }
        });
    }
});