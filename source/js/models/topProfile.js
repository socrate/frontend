var TopProfile = Backbone.Model.extend({
    defaults: {
        avatar: "",
        id: 0,
        last_name: "",
        level: 1,
        name: "",
        next_level_scores: 1,
        place: 1,
        scores: 0,
        status: ""
    },

    initialize: function() {
    }
});

var TopProfiles = Backbone.Collection.extend({
    model: TopProfile,

    fetch: function(callback) {
        callback = callback || function() {};
        var self = this;
        socrate.getTop('all', '', function(data, success, errors) {
            if (success) {
                var sortedData = _(data).sortBy(function(topItem) {
                    return topItem.place;
                });
                self.reset(sortedData);
                callback();
            } else {
                handleSocketErrors(errors);
            }
        });
    }
});