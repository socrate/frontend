var UserSuggest = Backbone.Model.extend({
    defaults: {
        '2gis_card_id': '',
        lat: 0,
        lon: 0,
        metrics: [{
            code: '',
            title: '',
            title_dat: '',
            value: 0
        }],
        title: ''
    }
});

var UserSuggests = Backbone.Collection.extend({
    model: UserSuggest,

    fetch: function(callback) {
        callback = callback || function() {};

        var self = this,
            authToken = user.get('auth_token');

        socrate.getUserSuggests(authToken, function(data, success, errors) {
            if (success) {
                self.reset(data);
                callback();
            } else {
                handleSocketErrors(errors);
            }
        });
    }
});
