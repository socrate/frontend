var BalloonView = Backbone.View.extend({
    template: jst['balloon'],

    initialize: function() {
        _.bindAll(this, 'render');
    },

    render: function() {
        var rawModel = this.model.toJSON(),
            preparedModel,
            html;

        preparedModel = {
                placeName: rawModel.title,
                stat: (function() {
                    var stats = [];

                    for (var i = 0, metricsCount = rawModel.metrics.length - 1; i < metricsCount; i++) {
                        stats.push({
                            statName: rawModel.metrics[i].title
                        });
                    }

                    return stats;
                })()
        };

        html = this.template(preparedModel);

        this.$el.html(html);

        return this;
    }
});
