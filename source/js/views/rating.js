var RatingView = Backbone.View.extend({
    tagName: 'li',

    className: 'rating__item',

    template: jst['ratingItem'],

    initialize: function() {
        _.bindAll(this, 'render');

        this.listenTo(this.model, "change", this.render);
    },

    render: function() {
        var html = this.template(this.model.attributes);
        this.$el.html(html);
        return this;
    }
});