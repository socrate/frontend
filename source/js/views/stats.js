var StatsView = Backbone.View.extend({
    template: jst['stats'],

    className: 'stats',

    initialize: function() {
        _.bindAll(this, 'render');
        this.listenTo(this.model, 'change', this.render);
    },

    render: function() {
        var total_metric = this.model.get('total_metric');
        this.$el.html(
            this.template(
                _.extend(this.model.toJSON(), {
                    total_metric: total_metric.toJSON()
                })
            )
        );

        var metrics = this.model.get('metrics');
        var statsListView = new StatsListView({ collection: metrics });
        this.$el.find('.stats__listContainer').html(statsListView.render().$el);

        return this;
    }
});