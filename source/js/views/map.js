var MapView = Backbone.View.extend({
    map: {},

    initialize: function() {
        _.bindAll(this, 'render');

        this.listenTo(this.collection, 'reset', this.updateMarkers);

        this.map = new DG.Map('map', {
            'center': new DG.LatLng(55.00888, 82.937937),
            'zoom': 12,
            'geoclicker': true,
            'locationControl': false,
            'fullscreenControl': false,
            'zoomControl': true
        });
    },

    updateMarkers: function() {
        var self = this;

        userSuggests.each(function(userSuggest) {
            var suggest = userSuggest.toJSON(),
                balloonView = new BalloonView({ model: userSuggest }),
                balloonHtml = balloonView.render().$el.html();

            DG.marker([suggest.lat, suggest.lon]).addTo(self.map).bindPopup(balloonHtml);
        });
    }
});

