/**
 * @requires $
 * @requires DG
 */
var AppView = Backbone.View.extend({
    template: jst['app'],
    el: $('body'),
    initialize: function() {
        _.bindAll(this, 'render', 'updateProfile', 'saveUserToCookie');

        this.listenTo(profile.get('total_metric'), 'change', this.updateProfile);
        this.listenTo(user, 'change', this.saveUserToCookie);

        this.render();
    },

    saveUserToCookie: function() {
        $.cookie('user_id', user.get('user_id'));
        $.cookie('auth_token', user.get('auth_token'));
    },

    updateProfile: function() {
        this.$el.find('.header__level').text(profile.get('total_metric').get('level'));
    },

    showSingle: function() {
        $('.single').show();
    },

    showApp: function() {
        // Hide everything but app
        $('.single').hide();
    },

    render: function() {
        var total_metric = profile.get('total_metric');
        var html = this.template({
            level: total_metric && total_metric.get('level')
        });

        if (!window.debug) {
            this.$el.html(html);

            this.$el.find('.pages').photor({
                onStart: function(p, target) {
                    // console.log(target);
                },
                onEnd: function(p) {
                    // console.log(p);
                }
            });

            var $pages = this.$el.find('.pages__viewportLayerPageIn');

            var $stats = $pages.eq(1);
            var statsView = new StatsView({ model: profile });
            $stats.html(statsView.render().$el);

            var $rating = $pages.eq(3);
            var ratingsView = new RatingsView({ collection: topProfiles});
            $rating.html(ratingsView.render().$el);

            if (typeof DG !== 'undefined') {
                DG.then(function () {
                    new MapView({ collection: userSuggests });
                });
            }
        } else {
            $('.pages').photor({
                onStart: function(p, target) {
                    // console.log(target);
                },
                onEnd: function(p) {
                    // console.log(p);
                }
            });


        }

        return this;
    }
});
