var StatsListView = Backbone.View.extend({
    tagName: 'ul',

    className: 'stats__list',

    initialize: function() {
        _.bindAll(this, 'render');

        this.listenTo(this.collection, "reset", this.render);
    },

    render: function() {
        this.$el.empty();

        var self = this;
        this.collection.each(function(stat) {
            var statView = new StatView({ model: stat});
            self.$el.append(statView.render().$el);
        });

        return this;
    }
});