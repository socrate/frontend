var RegisterView = Backbone.View.extend({
    template: jst['register'],

    // tagName: 'form',

    className: 'register',

    events: {
        'click .register__submit' : 'register'
    },

    initialize: function() {
        _.bindAll(this, 'render', 'register');
    },

    register: function() {
        var email = this.$el.find('.register__emailSelf').val();

        function doOnRegisterSuccess(data) {
            user.set(data);
            appView.showApp();
            appRouter.navigate('', {trigger: true});
        }

        var self = this;
        socrate.userRegister(email, 'password', function(data, success, errors) {
            if (success) {
                doOnRegisterSuccess(data);
            } else {
                socrate.userLogin(email, 'password', function(data, success, errors) {
                    if (success) {
                        doOnRegisterSuccess(data);
                    } else {
                        handleSocketErrors(errors);
                    }
                });
            }
        });
    },

    render: function() {
        var html = this.template();
        this.$el.html(html);
        return this;
    }
});