var SocialAuthView = Backbone.View.extend({
    template: jst['socialAuth'],

    initialize: function() {
        _.bindAll(this, 'render');

        this.listenTo(this.collection, "reset", this.render);

        this.collection.fetch();
    },

    render: function() {
        var preparedCollection = this.collection.map(function(social) {
            return social.toJSON();
        });
        var html = this.template({socials: preparedCollection});
        this.$el.html(html);
        return this;
    },
});