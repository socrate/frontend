var RatingsView = Backbone.View.extend({
    tagName: 'ol',

    className: 'rating',

    initialize: function() {
        _.bindAll(this, 'render');

        this.listenTo(this.collection, "reset", this.render);
    },

    render: function() {
        this.$el.empty();

        var self = this;
        this.collection.each(function(rating) {
            var ratingView = new RatingView({ model: rating});
            self.$el.append(ratingView.render().$el);
        });

        return this;
    }
});