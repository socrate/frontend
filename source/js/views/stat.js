var StatView = Backbone.View.extend({
    tagName: 'li',

    className: 'statItem',

    template: jst['statItem'],

    initialize: function() {
        _.bindAll(this, 'render');

        this.listenTo(this.model, "change", this.render);
    },

    render: function() {
        var percent = Math.floor(this.model.get('scores') / this.model.get('next_level_scores') * 100) || '0';
        var html = this.template({ percent: percent });
        this.$el.html(html);

        this.$el.attr('data-stat-name', this.model.get('title'));
        return this;
    }
});