function Chatter(master, host, port, route)
{
    var self = this;
    route = route || '/';
    this.master = master;
    this.host = host;
    this.port = port;
    this.route = route;
    this.socket = io('http://' + host + ':' + port + route);
    this.socket.on('package', function (data) {
        if(typeof self.callbacks[data.id - 1] == 'function')
        {
            self.callbacks[data.id - 1].call(self.master, data.response, data.success, data.errors);
        }
    });
    this.master.subscribe = function(group, eventName, callback, params)
    {
        return self.eventSubscribe(group, eventName, callback, params);
    };
}

Chatter.prototype.callbacks = [];
Chatter.prototype.eventCallbacks = [];

Chatter.prototype.eventSubscribe = function(group, eventName, callback, params)
{
    params = params || {};

    var idx = this.callbacks.push(callback);
    this.eventCallbacks[idx] = callback;

    this.callMethod('event', 'subscribe', {
        'group': group,
        'event': eventName,
        'params': params,
        'callback': idx
    }, function(){});
};

Chatter.prototype.applyMethod = function(group, method, args)
{
    var s = args.callee.toString();
    var sParams = s.match(/function \((.*)\)/g)[0].match(/\w+/g);

    var params = {};

    var callback = function(){};

    var i = 0;
    for(var k in sParams)
    {
        i += 1;

        var paramName = sParams[k];

        if(paramName == 'function')
        {
            continue;
        }
        var val = args[i - 2];

        if(paramName == 'callback')
        {
            callback = val;
            continue;
        }

        params[paramName] = val;
    }

    this.callMethod(group, method, params, callback);
};

Chatter.prototype.callMethod = function(group, method, params, callback)
{
    var id = this.callbacks.push(callback);
    this.socket.emit('package', {group: group, method: method, params: params, id: id});
};