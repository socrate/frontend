function Socrate(host, port)
{
    this.chatter = new Chatter(this, host, port);
}

// Test 1
Socrate.prototype.test1 = function(a, b, callback)
{
    this.chatter.applyMethod('test', 'test1', arguments);
};

// Регистрирует нового пользователя
Socrate.prototype.userRegister = function(email, password, callback)
{
    this.chatter.applyMethod('user', 'register', arguments);
};

// Авторизирует пользователя по паре email/пароль
Socrate.prototype.userLogin = function(email, password, callback)
{
    this.chatter.applyMethod('user', 'login', arguments);
};

// Вход в систему через авторизационный токен
Socrate.prototype.userTokenLogin = function(id, auth_token, callback)
{
    this.chatter.applyMethod('user', 'token_login', arguments);
};

// Вход в систему через аккаунт соц. сети
Socrate.prototype.userAccountLogin = function(provider, code, callback)
{
    this.chatter.applyMethod('user', 'account_login', arguments);
};

// [auth] Разлогиниваем пользователя
Socrate.prototype.userLogout = function(auth_token, callback)
{
    this.chatter.applyMethod('user', 'logout', arguments);
};

// Подтверждение e-mail адреса
Socrate.prototype.userConfirmEmail = function(email, code, callback)
{
    this.chatter.applyMethod('user', 'confirm_email', arguments);
};

// Запрос восстановления пароля
Socrate.prototype.userRestorePasswordRequest = function(email, callback)
{
    this.chatter.applyMethod('user', 'restore_password_request', arguments);
};

// Вводим код, который упал на почту при восстановлении пароля, а также новый пароль
Socrate.prototype.userRestorePasswordCode = function(email, code, new_password, callback)
{
    this.chatter.applyMethod('user', 'restore_password_code', arguments);
};

// [auth] Изменение профиля пользователя
Socrate.prototype.userUpdateProfile = function(auth_token, callback)
{
    this.chatter.applyMethod('user', 'update_profile', arguments);
};

// [auth] Изменение пароля пользователя
Socrate.prototype.userChangePassword = function(auth_token, old_password, new_password, callback)
{
    this.chatter.applyMethod('user', 'change_password', arguments);
};

// Получение профиля пользователя
Socrate.prototype.getUserProfile = function(auth_token, user_id, user_login, callback)
{
    this.chatter.applyMethod('user', 'get_profile', arguments);
};

// Получаем список возможных привязок аккаунтов
Socrate.prototype.listUserAuthTypes = function(auth_token, with_auth_urls, callback)
{
    this.chatter.applyMethod('user', 'list_auth_types', arguments);
};

// [auth] Привязываем новый аккаунт
Socrate.prototype.userAttachAccount = function(auth_token, provider, code, callback)
{
    this.chatter.applyMethod('user', 'attach_account', arguments);
};

// [auth] Получаем садджесты для карты
Socrate.prototype.getUserSuggests = function(auth_token, callback)
{
    this.chatter.applyMethod('user', 'get_suggests', arguments);
};

// [auth] Устанавливаем новый дефолтный город
Socrate.prototype.setUserCity = function(auth_token, city_code, callback)
{
    this.chatter.applyMethod('user', 'set_city', arguments);
};

// [auth] Отмечаем геолокацию пользователя (желательно после каждой авторизации + переодически, когда координаты сильно отличаются, чтобы собирать эту инфу)
Socrate.prototype.userCheckin = function(auth_token, lat, lon, callback)
{
    this.chatter.applyMethod('user', 'checkin', arguments);
};

// Получаем список городов
Socrate.prototype.getCities = function(callback)
{
    this.chatter.applyMethod('cities', 'get', arguments);
};

// Получам ТОП всех пользователей или друзей по общему рейтингу или специализированному)
Socrate.prototype.getTop = function(users_type, provider_code, callback)
{
    this.chatter.applyMethod('top', 'get', arguments);
};
