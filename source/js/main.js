function handleSocketErrors(errors) {
    console.error(errors);
}


(function(w) {
    var path = window.location.pathname;
    if (path != '/') {
        window.location.replace('/#'+path.substr(1)+window.location.search);
    }
})(window);

$(function() {

    // window.socrate = new Socrate('socrate.me', 80, 'api');
    window.socrate = new Socrate('37.193.93.131', 3000, 'api');

    var initApp = _.once(function() {
        window.appView = new AppView();

        window.appRouter = new Router();
        Backbone.history.start();
    });

    socrate.chatter.socket.on('connect', function() {
        console.warn('CONNECTED');

        window.user = new User();
        window.profile = new Profile();
        window.topProfiles = new TopProfiles();
        window.userSuggests = new UserSuggests();
        window.socials = new Socials();

        // Async event subscribe
        socrate.subscribe('client', 'push', function(data) {
            console.warn('Subscription', data);
        });

        function resetMetrics(data) {
            console.log(arguments);
            profile.fetch();
        }

        socrate.subscribe('user', 'account_attach_start', resetMetrics);
        socrate.subscribe('user', 'account_attach_progress', resetMetrics);
        socrate.subscribe('user', 'account_attach_stop', resetMetrics);
        socrate.subscribe('user', 'account_attach_status', resetMetrics);

        initApp();

        $('.achievements__item').on('click', function() {
            $(this).toggleClass('_flipped');
        });
    });

    socrate.chatter.socket.on('disconnect', function() {
        console.warn('DISCONNECTED');
    });

    socrate.chatter.socket.on('error', function() {
        console.warn('ERROR', arguments);
    });
});

