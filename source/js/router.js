var Router = Backbone.Router.extend({
    routes: {
        'login': 'login',
        'connect': 'connect',
        'user/auth/:provider(?:params)': 'socialCallback',
        '*path' : 'defaultApp'
    },

    initialize: function() {
        _.bindAll(this, 'defaultApp', 'login', 'connect', 'socialCallback', 'validateSession', 'validateSocials', 'fetchRequirements');
    },

    defaultApp: function() {
        var self = this;

        if (self.validateSession()) {
            self.fetchRequirements([socials, profile, topProfiles, userSuggests], function() {
                self.validateSocials(function(valid) {
                    if (valid) {
                        appView.showApp();
                        self.navigate('', {replace: true});
                    } else {
                        self.connect();
                    }
                });
            });
        } else {
            self.login();
        }
    },

    socialCallback: function(provider, params) {
        var self = this;
        if (self.validateSession()) {
            self.fetchRequirements([socials], function() {
                params = params || '';
                var paramsStrings = params.split('&');
                var values = _(paramsStrings).map(function(paramString) {
                    return paramString.split('=')[1];
                });
                var code = values.join(',');
                socials.findWhere({ short_code: provider }).set('is_attached', true);
                socrate.userAttachAccount(user.get('auth_token'), provider, code, function(data, success, errors) {
                    self.defaultApp();
                });
            });
        } else {
            self.login();
        }
    },

    connect: function() {
        if (this.validateSession()) {
            var socialAuthView = new SocialAuthView({ collection: socials });
            $('.single').html(socialAuthView.render().$el);
            appView.showSingle();
            this.navigate('connect', {replace: true});
        } else {
            this.login();
        }
    },

    login: function() {
        var registerView = new RegisterView();
        $('.single').html(registerView.render().$el);
        appView.showSingle();
        this.navigate('login', {replace: true});
    },

    validateSession: function() {
        return !!user.get('auth_token');
    },

    validateSocials: function(callback) {
        socials.fetch(function() {
            var hasAuth = socials.any(function(social) {
                return social.get('is_attached');
            });
            callback(hasAuth);
        });
    },

    fetchRequirements: function(requirements, callback) {
        requirements = requirements || [];
        var fetchedCount = 0;
        var fetchSize = requirements.length;

        _(requirements).each(function(r) {
            r.fetch(afterEachFetch);
        });

        function afterEachFetch() {
            fetchedCount++;
            if (fetchedCount == fetchSize) {
                callback();
            }
        }
    }
});