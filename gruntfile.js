module.exports = function(grunt) {

    var minify = require('html-minifier').minify;

    // Project configuration.
    grunt.initConfig({

        clean: {
            regular: ['www/assets/*', 'temp/*']
        },

        handlebars: {
            regular: {
                options: {
                    namespace: "jst",
                    processName: function(filepath) {
                        return filepath.replace(/source\/templates\/(.*).html/g, "$1");
                    },
                    processContent: function(content, filepath) {
                        return minify(content, {
                            removeComments: true,
                            collapseWhitespace: true,
                            conservativeCollapse: true,
                            removeEmptyAttributes: true
                        });
                    }
                },
                src: [
                    'source/templates/*.html'
                ],
                dest: 'temp/jst.js'
            }
        },

        concat: {
            less: {
                src: [
                    'source/less/reset.less',
                    'source/less/fonts.less',
                    'source/less/mixins.less',
                    'source/less/common.less',
                    'source/blocks/*/*.less'
                ],
                dest: 'temp/application.less'
            },
            js: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/jquery.cookie/jquery.cookie.js',
                    'bower_components/handlebars/handlebars.js',
                    'temp/jst.js',
                    'source/js/socketio.js',
                    'bower_components/underscore/underscore.js',
                    'bower_components/backbone/backbone.js',
                    'source/js/chatter.js',
                    'source/js/socrate.js',
                    'source/js/views/*.js',
                    'source/js/models/*.js',
                    'source/blocks/*/*.js',
                    'source/js/router.js',
                    'source/js/main.js',
                    'source/js/photor.js'
                ],
                dest: 'www/assets/application.js',
            }
        },

        uglify: {
            application: {
                files: {
                    'www/assets/application.js': ['www/assets/application.js']
                }
            }
        },

        less: {
            application: {
                files: {
                    'www/assets/application.css': ['temp/application.less']
                }
            }
        },

        csso: {
            application: {
                files: {
                    'www/assets/application.css': ['www/assets/application.css']
                }
            }
        },

        copy: {
            images: {
                expand: true,
                src: [
                    'source/images/**/*.{jpg,jpeg,gif,png,svg}',
                    '!source/images/**/_*'
                ],
                dest: 'www/assets/images/',
                rename: function(destBase, destPath) {
                    destPath = destPath.replace('source/images/', '');
                    return destBase + destPath;
                }
            },
            svg: {
                expand: true,
                src: [
                    'source/svg/**/*.svg',
                    '!source/svg/_*'
                ],
                dest: 'www/assets/svg/',
                rename: function(destBase, destPath) {
                    destPath = destPath.replace('source/svg/', '');
                    return destBase + destPath;
                }
            },
            fonts: {
                expand: true,
                src: [
                    'source/fonts/**/*.{woff,svg,eot}',
                    '!source/fonts/_*'
                ],
                dest: 'www/assets/fonts/',
                rename: function(destBase, destPath) {
                    destPath = destPath.replace('source/fonts/', '');
                    return destBase + destPath;
                }
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            app: [
                'source/blocks/**/*.js',
                'source/js/*.js',
                'gruntfile.js'
            ]
        },

        watch: {
            css: {
                files: [
                    'source/blocks/**/*.less',
                    'source/less/**/*.less'
                ],
                tasks: ['css'],
                options: {
                    spawn: false,
                    interrupt: true
                }
            },
            js: {
                files: [
                    'source/blocks/**/*.js',
                    'source/js/**/*.js'
                ],
                tasks: ['js'],
                options: {
                    spawn: false,
                    interrupt: true
                }
            },
            images: {
                files: [
                    'source/images/**/*.{jpg,jpeg,gif,png}',
                    'source/svg/*.svg'
                ],
                tasks: ['img'],
                options: {
                    spawn: false,
                    interrupt: true
                }
            }
        },

        svgmin: {
            options: {},
            dist: {
                files: [{
                    expand: true,
                    src: [
                        'source/svg/*.svg',
                        'source/svg/*/*.svg',
                        '!source/svg/_*'
                    ],
                    dest: 'www/assets/svg/',
                    rename: function(destBase, destPath) {
                        destPath = destPath.replace('source/svg/', '');
                        return destBase + destPath;
                    }
                }]
            }
        },

        connect: {
            server: {
                options: {
                    port: 3000,
                    base: 'www/'
                }
            }
        },

        dataUri: {
            dist: {
                src: ['www/assets/mkb.css'],
                dest: 'www/assets/',
                options: {
                    target: ['**/*.*'],
                    fixDirLevel: true,
                    baseDir: 'www/assets/'
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-csso');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-data-uri');

    // Tasks

    grunt.registerTask('img', "Build images", function() {
        var tasks;

        if (grunt.option('release')) {
            tasks = ['copy:images', 'svgmin'];
        } else {
            tasks = ['copy:images', 'copy:svg'];
        }
        grunt.task.run(tasks);
    });

    grunt.registerTask('css', "Build styles", function() {
        var tasks = ['concat:less',  'less', 'dataUri'];

        if (grunt.option('release')) {
            tasks.push('csso');
        }
        grunt.task.run(tasks);
    });

    grunt.registerTask('js', "Build scripts", function() {
        var tasks = ['handlebars', 'concat:js', 'jshint'];

        if (grunt.option('release')) {
            tasks.push('uglify');
        }
        grunt.task.run(tasks);
    });

    grunt.registerTask('fonts', "Copy fonts", function() {
        var tasks = ['copy:fonts'];

        grunt.task.run(tasks);
    });

    grunt.registerTask('default', [
        'clean:regular',
        'img',
        'fonts',
        'css',
        'js'
    ]);

    grunt.registerTask('dev', ['default', 'connect', 'watch']);

};